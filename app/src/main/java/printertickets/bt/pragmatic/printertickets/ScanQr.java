package printertickets.bt.pragmatic.printertickets;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.Result;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import printertickets.bt.pragmatic.printertickets.Model.GlobalVariables;
import printertickets.bt.pragmatic.printertickets.Model.Productos;
import printertickets.bt.pragmatic.printertickets.Model.ProductosRepositorio;

public class ScanQr extends Activity implements ZXingScannerView.ResultHandler {

   private ZXingScannerView mScannerView;
   LinearLayout myLInearLayout;
   RelativeLayout parentLayout;
   RelativeLayout relative;

   Button cancel;
   Button close;
   Button enter;
   Button out;
   TextView text;
   View scan;
   List<String[]> rows = new ArrayList<String[]>();

   View viewScan;

   @Override
   public void onCreate(Bundle state) {

      super.onCreate(state);

      //cancel = (Button) findViewById(R.id.cancel);
      //close = (Button) findViewById(R.id.close);
      //enter = (Button) findViewById(R.id.enter);
      //out = (Button) findViewById(R.id.out);
      //text = (TextView) findViewById(R.id.text);
      //relative = (RelativeLayout) findViewById(R.id.scanner);

      this.runOnUiThread(new Runnable() {
         public void run() {

            //Parent
            parentLayout = new RelativeLayout(ScanQr.this);
            parentLayout.setBackgroundColor(Color.BLACK);
            RelativeLayout.LayoutParams paramsParent = new RelativeLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
            parentLayout.setLayoutParams(paramsParent);


            mScannerView = new ZXingScannerView(ScanQr.this);   // Programmatically initialize the scanner view

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                  FrameLayout.LayoutParams.MATCH_PARENT,
                  FrameLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(100, 100, 20, 60);
            params.gravity = Gravity.CENTER_HORIZONTAL;

            //sv.addView(mScannerView);

            Button btnTag = new Button(ScanQr.this);
            btnTag.setGravity(20);
            btnTag.setText("Cancelar");
            btnTag.setTextColor(Color.WHITE);
            btnTag.setBackgroundColor(Color.TRANSPARENT);
            //btnTag.setBackgroundColor(Color.BLUE);
            btnTag.setPadding(0, 0, 0, 0);


            ActionBar.LayoutParams layoutParamsButton=new ActionBar.LayoutParams(230, 90);
            layoutParamsButton.setMargins(44, 44, 44, 44);
            btnTag.setLayoutParams(layoutParamsButton);
            btnTag.setY(Math.round(height * 0.020));
            btnTag.setX(Math.round(width * 0.72));
            btnTag.setGravity(Gravity.CENTER_VERTICAL | Gravity.START);


            Button btnManual = new Button(ScanQr.this);
            btnManual.setGravity(20);
            btnManual.setText("Ingreso manual");
            btnManual.setTextColor(Color.WHITE);
            btnManual.setBackgroundColor(Color.TRANSPARENT);
            //btnTag.setBackgroundColor(Color.BLUE);
            btnManual.setPadding(0, 0, 0, 0);


            ActionBar.LayoutParams layoutParamsButton2=new ActionBar.LayoutParams(width, 90);
            layoutParamsButton2.gravity = Gravity.CENTER;
            layoutParamsButton2.setMargins(44, 44, 44, 44);
            btnManual.setLayoutParams(layoutParamsButton2);
            btnManual.setY(Math.round(height * 0.85));
            btnManual.setX(Math.round(width * 0.0));
            btnManual.setGravity(Gravity.CENTER);
            // btnManual.setGravity(Gravity.CENTER_VERTICAL);

            //btnManual.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.START);
            //btnManual.setGravity(Gravity.CENTER | Gravity.START);


            btnTag.setOnClickListener(new View.OnClickListener()
            {
               @Override
               public void onClick(View v) {
                  exit();
               }
            });

            btnManual.setOnClickListener(new View.OnClickListener()
            {
               @Override
               public void onClick(View v) {
                  Intent intent = new Intent(ScanQr.this, IngresoCodigoManual.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                  startActivity(intent);
               }
            });

            setContentView(parentLayout);
            parentLayout.addView(mScannerView);

            parentLayout.addView(btnTag);
            parentLayout.addView(btnManual);

            //LayoutInflater inflater =(LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //View myView1 = inflater.inflate(R.layout.scan, null);
            //View myView2 = inflater.inflate(R.layout.activity_panchang_cal_inflate, null);

            //parentLayout.addView(myView1);
            //linearday.addView(myView2);

         }
      });


   }

   @Override
   public void onResume() {
      super.onResume();
      mScannerView.setResultHandler(ScanQr.this); // Register ourselves as a handler for scan results.
      mScannerView.startCamera();          // Start camera on resume
   }

   @Override
   public void onPause() {
      super.onPause();
      mScannerView.stopCamera();           // Stop camera on pause
   }
   @Override
   public void handleResult(Result rawResult) {
      Log.i("resultadoScan ",String.valueOf(rawResult));
      GlobalVariables.setFinishScan(true);
      GlobalVariables.setCode(String.valueOf(rawResult));

      this.mScannerView.stopCameraPreview();

      if (readDataBase(String.valueOf(rawResult))) {
         for (int i = 0; i < this.rows.size(); ++i) {
            if (this.rows.get(i)[4].equals(String.valueOf(rawResult)))
            {
               int id = Integer.parseInt(this.rows.get(i)[0]);
               String nombre = this.rows.get(i)[1];
               Double precio = Double.parseDouble(this.rows.get(i)[2]);
               //int cantidad = Integer.parseInt(this.rows.get(i)[3]);
               agregarProducto(new Productos(id, nombre, precio, 1, (precio * 1), 0), 1);
               this.finish();
            }
            else
            {

            }
         }
      } else {
         new AlertDialog.Builder((Context)this).setTitle("Agregar Producto").setMessage("No se ha encontrado el producto solicitado").setPositiveButton("Aceptar", new DialogInterface.OnClickListener(){

            public void onClick(DialogInterface dialogInterface, int n) {
               ScanQr.this.mScannerView.resumeCameraPreview(ScanQr.this);
            }
         }).create().show();
      }

      //mScannerView.resumeCameraPreview(this);
      //finish();
   }

   private boolean readDataBase(String string2) {
      CSVReader cSVReader = new CSVReader((Context)this, "database.csv");
      try {
         this.rows = cSVReader.readCSV();
      }
      catch (IOException iOException) {
         iOException.printStackTrace();
      }
      for (int i = 0; i < this.rows.size(); ++i) {
         Log.i((String)"row", (String)String.format("row %s: %s, %s, %s, %s, %s", i, this.rows.get(i)[0], this.rows.get(i)[1], this.rows.get(i)[2], this.rows.get(i)[3], this.rows.get(i)[4]));
         if (string2.equals(this.rows.get(i)[4])) {
            Log.i((String)"RES ", (String)"SE ENCONTRO PRODUCTO");
            return true;
         }
         Log.i((String)"RES ", (String)"NO SE ENCONTRO PRODUCTO");
      }
      return false;
   }


   public void agregarProducto(Productos productos, int n) {
      int n2 = 0;
      boolean bl = false;
      if (ProductosRepositorio.getInstance().productos.size() == 0) {
         ProductosRepositorio.getInstance().productos.add(productos);
         return;
      }
      for (Productos productos2 : ProductosRepositorio.getInstance().productos) {
         ++n2;
         if (productos2.getId() == productos.getId()) {
            productos2.setCantidad(productos2.getCantidad() + n);
            productos2.setTotal((double)productos2.getCantidad() * productos2.getPrecio());
            bl = false;
            break;
         }
         bl = true;
      }
      if (bl) {
         ProductosRepositorio.getInstance().productos.add(productos);
      }
   }


   private void readDataBase()
   {
      List<String[]> rows = new ArrayList<>();
      CSVReader csvReader = new CSVReader(this, "database.csv");
      try {
         rows = csvReader.readCSV();
      } catch (IOException e) {
         e.printStackTrace();
      }

      for (int i = 0; i < rows.size(); i++) {
         Log.i("row", String.format("row %s: %s, %s, %s, %s", i, rows.get(i)[0], rows.get(i)[1], rows.get(i)[2], rows.get(i)[3] ));
      }
   }

   public void onBackPressed(){
      exit();
   }

   public void exit()
   {
      GlobalVariables.setFinishScan(true);
      GlobalVariables.setCode("cancel");

      mScannerView.resumeCameraPreview(this);
      finish();
   }

}



//crear articulo de manera manual
//crear articulo de manera con codigo de barras

//2da pantalla

//Venta de articulos, busqueda  por campo
// buscarlo por codigo tecleada

