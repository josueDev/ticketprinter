
package printertickets.bt.pragmatic.printertickets;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ConfiguracionGenerales extends AppCompatActivity {
    EditText contacto;
    EditText direccion;
    EditText nombre;
    Button buttonGuardar;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.generales);
        this.nombre = (EditText)this.findViewById(R.id.editText);
        this.direccion = (EditText)this.findViewById(R.id.editText2);
        this.contacto = (EditText)this.findViewById(R.id.editText3);
        this.buttonGuardar = this.findViewById(R.id.buttonGuardarConf);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SharedPreferences preferences = this.getApplicationContext().getSharedPreferences("MyPref", 0);
        preferences.edit();
        String name = preferences.getString("nombre_establecimiento", "Nombre por defecto");
        String direc = preferences.getString("direccion_establecimiento", "Direccion por defecto");
        String contact = preferences.getString("contacto_establecimiento", "Contacto por defecto");

        if (name.equals("Nombre por defecto"))
        {

        }
        else
        {
            nombre.setText(name);
        }

        if (direc.equals("Direccion por defecto"))
        {

        }
        else
        {
            direccion.setText(direc);
        }

        if (contact.equals("Contacto por defecto"))
        {

        }
        else
        {
            contacto.setText(contact);
        }

        buttonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                guardarDatos();
            }
        });

    }

    public void guardarDatos() {
        SharedPreferences.Editor editor = this.getApplicationContext().getSharedPreferences("MyPref", 0).edit();
        editor.putString("nombre_establecimiento", this.nombre.getText().toString());
        editor.putString("direccion_establecimiento", this.direccion.getText().toString());
        editor.putString("contacto_establecimiento", this.contacto.getText().toString());
        editor.commit();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        this.finish();
        return true;
    }
}

