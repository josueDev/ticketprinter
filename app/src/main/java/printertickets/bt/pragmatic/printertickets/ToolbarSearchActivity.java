package printertickets.bt.pragmatic.printertickets;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.support.v7.widget.SearchView;
import android.widget.SimpleAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import printertickets.bt.pragmatic.printertickets.Model.Productos;
import printertickets.bt.pragmatic.printertickets.Model.ProductosRepositorio;

public class ToolbarSearchActivity extends AppCompatActivity implements SearchView.OnQueryTextListener{

    private String TAG = "FILTER ";
    ArrayList<HashMap<String, String>> arrayList = new ArrayList();
    HashMap<String, String> map;
    List<String[]> rows = new ArrayList<String[]>();
    SimpleAdapter simpleAdapter;
    private ListView simpleListView;
    private Toolbar tbMainSearch;

    @Override
    protected void onCreate(Bundle object) {
        super.onCreate((Bundle)object);
        this.setContentView(R.layout.activity_toolbar_search);
        this.readDataBase();

        int to[]={R.id.textView};
        this.tbMainSearch = (Toolbar)((Object)this.findViewById(R.id.tb_toolbarsearch));
        this.simpleListView = (ListView)this.findViewById(R.id.lv_toolbarsearch);
        this.simpleAdapter = new SimpleAdapter(ToolbarSearchActivity.this, this.arrayList, R.layout.custom_list_items, new String[]{"nombre", "codigobarras"}, to);
        this.simpleListView.setAdapter(this.simpleAdapter);
        this.setSupportActionBar(this.tbMainSearch);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.simpleAdapter.notifyDataSetChanged();
        this.simpleListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            public void onItemClick(AdapterView<?> object, View view, int n, long l) {
                Log.i((String)"postionList", (String)String.valueOf(n));
                Log.i((String)"ID", (String)String.valueOf(l));
                final HashMap obj = (HashMap)ToolbarSearchActivity.this.simpleAdapter.getItem(n);
                System.out.println(obj.get("codigobarras"));
                final EditText editText = new EditText(ToolbarSearchActivity.this);
                    new AlertDialog.Builder((ToolbarSearchActivity.this)).setTitle("Agregar Producto").setMessage("\u00bfCu\u00e1l es la cantidad de productos que desea agregar?").setView((View)editText).setPositiveButton("Agregar", new DialogInterface.OnClickListener(){

                    public void onClick(DialogInterface object, int n) {
                        for (int i = 0; i < ToolbarSearchActivity.this.rows.size(); ++i) {
                            if (rows.get(i)[4].equals(obj.get("codigobarras")))
                            {
                                System.out.println("CREATE PRODUCTO " + obj.get("codigobarras"));
                                int id = Integer.parseInt(ToolbarSearchActivity.this.rows.get(i)[0]);
                                Object nombre = ToolbarSearchActivity.this.rows.get(i)[1];
                                Double precio = Double.parseDouble(ToolbarSearchActivity.this.rows.get(i)[2]);
                                int cantidad = Integer.valueOf(editText.getText().toString());

                                Productos producto = new Productos(id, (String)nombre, precio, cantidad, precio * (double)cantidad, 0);
                                agregarProducto(producto, cantidad);
                                ToolbarSearchActivity.this.finish();
                            }

                        }

                    }
                }).setNegativeButton("Cancelar", null).create().show();
                editText.setInputType(2);
                editText.setGravity(1);
                showKeyboard(editText);
            }

        });
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void agregarProducto(Productos productos, int n) {
        int n2 = 0;
        boolean bl = false;
        if (ProductosRepositorio.getInstance().productos.size() == 0) {
            ProductosRepositorio.getInstance().productos.add(productos);
            return;
        }
        for (Productos productos2 : ProductosRepositorio.getInstance().productos) {
            ++n2;
            if (productos2.getId() == productos.getId()) {
                productos2.setCantidad(productos2.getCantidad() + n);
                productos2.setTotal((double)productos2.getCantidad() * productos2.getPrecio());
                bl = false;
                break;
            }
            bl = true;
        }
        if (bl) {
            ProductosRepositorio.getInstance().productos.add(productos);
        }
    }

    private void readDataBase() {
        this.rows = new ArrayList<String[]>();
        CSVReader cSVReader = new CSVReader((Context)this, "database.csv");
        try {
            this.rows = cSVReader.readCSV();
        }
        catch (IOException iOException) {
            iOException.printStackTrace();
        }
        for (int i = 0; i < this.rows.size(); ++i) {
            Log.i((String)"row", (String)String.format("row %s: %s, %s, %s, %s", i, this.rows.get(i)[0], this.rows.get(i)[1], this.rows.get(i)[2], this.rows.get(i)[3]));
            this.map = new HashMap();
            this.map.put("nombre", this.rows.get(i)[1]);
            this.map.put("codigobarras", this.rows.get(i)[4]);
            this.arrayList.add(this.map);
        }
    }

    public void showKeyboard(final EditText ettext){
        ettext.requestFocus();
        ettext.postDelayed(new Runnable(){
                               @Override public void run(){
                                   InputMethodManager keyboard=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext,0);
                               }
                           }
                ,200);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    /*
    private void setUpViews() {
        tbMainSearch = (Toolbar)findViewById(R.id.tb_toolbarsearch);
        lvToolbarSerch =(ListView) findViewById(R.id.lv_toolbarsearch);
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,arrays);
        lvToolbarSerch.setAdapter(adapter);
        setSupportActionBar(tbMainSearch);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
    */

    @SuppressLint("ResourceType")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.layout.menu_search, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem mSearchmenuItem = menu.findItem(R.id.menu_toolbarsearch);
        SearchView searchView = (SearchView) mSearchmenuItem.getActionView();
        searchView.setQueryHint("Buscar artículo");
        searchView.setOnQueryTextListener(this  );
        Log.d(TAG, "onCreateOptionsMenu: mSearchmenuItem->" + mSearchmenuItem.getActionView());
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.d(TAG, "onQueryTextSubmit: query->"+query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String string2) {
        String string3 = this.TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onQueryTextChange: newText->");
        stringBuilder.append(string2);
        Log.i((String)string3, (String)stringBuilder.toString());
        this.simpleAdapter.getFilter().filter((CharSequence)string2);
        return true;
    }

}