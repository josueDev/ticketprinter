package printertickets.bt.pragmatic.printertickets;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import printertickets.bt.pragmatic.printertickets.Model.Productos;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

    private List<Productos> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    MyRecyclerViewAdapter(Context context, List<Productos> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Productos dataProducto  = mData.get(position);

        String precio = Double.toString(  dataProducto.getPrecio()) ;
        String nombre =  dataProducto.getNombre();
        int cantidad = dataProducto.getCantidad();
        String total = Double.toString(  dataProducto.getTotal()) ;


        holder.precio_producto.setText(precio);
        holder.nombre_producto.setText(nombre);
        //holder.cantidad_producto.setText(cantidad);
        holder.cantidad_producto.setText(String.format("%d", cantidad));
        holder.total_producto.setText(total);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView precio_producto;
        TextView nombre_producto;
        TextView cantidad_producto;
        TextView total_producto;

        ViewHolder(View itemView) {
            super(itemView);
            precio_producto = itemView.findViewById(R.id.precio_producto);
            nombre_producto  = itemView.findViewById(R.id.nombre_producto);
            cantidad_producto  = itemView.findViewById(R.id.cantidad_producto);
            total_producto = itemView.findViewById(R.id.total_producto);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    Double getItem(Productos id) {
        return id.getPrecio();
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}