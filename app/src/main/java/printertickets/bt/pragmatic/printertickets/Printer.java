package printertickets.bt.pragmatic.printertickets;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import printertickets.bt.pragmatic.printertickets.Model.Globals;


public class Printer extends AppCompatActivity {

    Button desconectar,buttonListDevices;
    ImageButton encender;
    private ProgressDialog progress;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    private boolean isBtConnected = false;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    public String addressBluetooth;
    public String nameBluetooth;
    String dataPrint;
    TextView textViewName,textViewAddress,textViewStatus;
    int intentReconnect = 0;
    boolean comesIntent = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vista1);
        encender =  findViewById(R.id.encender);
        desconectar =  findViewById(R.id.desconectar);
        textViewName = findViewById(R.id.textViewName);
        textViewAddress = findViewById(R.id.textViewAddress);
        textViewStatus =  findViewById(R.id.textViewStatus);
        buttonListDevices = findViewById(R.id.button_list_devices);

        dataPrint = getIntent().getStringExtra("dataPrint");
        Globals sharedData = Globals.getInstance();
        sharedData.setValue(dataPrint);

        if (!(dataPrint==null))
        {
            comesIntent = true;
        }

        encender.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                pressButtonPrinter();
            }
        });

        desconectar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("addressBluetooth", "device_address");
                editor.putString("nameBluetooth", "device_name");
                editor.commit();

                addressBluetooth = "device_address";
                nameBluetooth = "device_name";

                Disconnect(); //close connection
            }
        });

        buttonListDevices.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(Printer.this, ListDevices.class);
                startActivity(i);
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void initializeBt()
    {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        addressBluetooth = preferences.getString("addressBluetooth","device_address");
        nameBluetooth = preferences.getString("nameBluetooth","device_name");
        Log.i("address: ", addressBluetooth);
        Log.i("name: ", nameBluetooth);

        if ((nameBluetooth.equals("")) || (nameBluetooth.equals("device_name")))
        {
            textViewName.setText("Nombre no disponible \n");
        }
        else
        {
            textViewName.setText(nameBluetooth);
        }

        if ((addressBluetooth.equals("")) || (addressBluetooth.equals("device_address")))
        {
            msg("No tiene un dispositivo bluetooth asociado");
            textViewStatus.setText("Status: Desconectado");
            textViewStatus.setTextColor(Color.RED);
            textViewAddress.setText("MAC Address no disponible");

        }
        else
        {
            new ConnectBT().execute();
        }


    }

    private void Disconnect()
    {
        if (btSocket!=null) //If the btSocket is busy
        {
            try
            {
                isBtConnected = false;
                btSocket.close(); //close connection

                textViewName.setText("Nombre no disponible \n");
                textViewAddress.setText("MAC Address no disponible");
                textViewStatus.setText("Status: Desconectado");
                textViewStatus.setTextColor(Color.RED);

            }
            catch (IOException e)
            { msg("Error");}
        }
        //finish(); //return to the first layout

    }

    private void pressButtonPrinter()
    {
        if (isBtConnected==false)
        {
            initializeBt();
        }
        else
        {
            sendDataPrint();
        }
    }

    private void msg(String s)
    {
        Toast.makeText(getApplicationContext(),s, Toast.LENGTH_LONG).show();
    }

    private class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true;

        @Override
        protected void onPreExecute()
        {
            Printer.this.runOnUiThread(new Runnable() {
                public void run() {
                   //progress = ProgressDialog.show(Printer.this, "Conectando...", "Espere por favor");
                }
            });

            progress = ProgressDialog.show(Printer.this, "Conectando...", "Espere por favor");

            //progress.dismiss();
        }

        @Override
        protected Void doInBackground(Void... devices)
        {
            try
            {


                if (btSocket == null || !isBtConnected)
                {
                    //progress.dismiss();
                    myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(addressBluetooth);//connects to the device's address and checks if it's available
                    btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();//start connection

                    Printer.this.runOnUiThread(new Runnable(){

                        @Override
                        public void run() {
                            textViewAddress.setText("MAC Address: " + addressBluetooth);
                            textViewStatus.setTextColor(Color.BLUE);
                            textViewStatus.setText("Status: Conectado");

                        }
                    });

                }
            }
            catch (IOException e)
            {

                Printer.this.runOnUiThread(new Runnable(){

                    @Override
                    public void run() {

                        textViewAddress.setText("MAC Address: " + addressBluetooth);
                        textViewStatus.setTextColor(Color.RED);
                        textViewStatus.setText("Status: Desconectado");
                        progress.dismiss();

                    }
                });

                ConnectSuccess = false;
                isBtConnected = false;


            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);

            if (!ConnectSuccess)
            {
                msg("Conexión fallida, por favor verifique su dispositivo Bluetooth");
                isBtConnected = false;
                //comesIntent = false;
                //finish();
            }
            else
            {
                msg("Dispositivo Conectado");
                isBtConnected = true;

                if (comesIntent == true)
                {
                    //comesIntent = false;
                    pressButtonPrinter();
                }
            }
            progress.dismiss();

        }
    }

    @Override
    public void onResume() {
        Log.e("DEBUG", "onResume of HomeFragment");
        super.onResume();

        dataPrint = getIntent().getStringExtra("dataPrint");
        Globals sharedData = Globals.getInstance();
        sharedData.setValue(dataPrint);

        initializeBt();
        /*
        Log.i("isBtConnected ", String.valueOf(isBtConnected));
        intentReconnect++;
        if (isBtConnected==false) //If the btSocket is busy
        {
            if (intentReconnect == 2)
            {

            }
            else
            {
                initializeBt();
            }
        }
        */
    }

    @Override
    public void onPause() {
        super.onPause();
        //Disconnect();
    }

    @Override
    public void onStop() {
        super.onStop();
        Disconnect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Disconnect();
    }

    public void sendDataPrint()
    {

        Thread t = new Thread() {
            public void run() {
                try {
                    OutputStream fos = btSocket.getOutputStream();
                    InputStream is = btSocket.getInputStream();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    int size = 0;
                    do
                    {
                        size = is.available();
                        if (size > 0)
                        {
                            is.read();
                        }
                    }while (size > 0 );

                    if (dataPrint.toString().equals(""))
                    {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                msg("No hay datos para imprimir");
                            }
                        });
                    }
                    else
                    {

                        baos.write(dataPrint.getBytes());
                        fos.write(baos.toByteArray());
                        fos.flush();
                        finish();
                    }

                } catch (Exception e) {
                    Log.e("Main", "Exe ", e);

                    runOnUiThread(new Runnable() {
                        public void run() {
                            msg("Ha ocurrido un error al imprimir");
                        }
                    });
                }
            }
        };
        t.start();
    }
}