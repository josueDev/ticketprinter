package printertickets.bt.pragmatic.printertickets;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import android.support.v7.widget.RecyclerView.ViewHolder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import printertickets.bt.pragmatic.printertickets.Model.Productos;
import printertickets.bt.pragmatic.printertickets.Model.ProductosRepositorio;

public class MainActivity extends AppCompatActivity implements MyRecyclerViewAdapter.ItemClickListener {

    private RecyclerView mRecyclerView;
    MyRecyclerViewAdapter adapter;
    private Button buttonScan, buttonImprimir, buttonProductos,buttonConfiguracion, buttonNuevoProducto;
    private TextView textViewResult;
    public int idProductoExterno = 1;
    public Toolbar appToolBar;
    ProductosRepositorio repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        textViewResult = findViewById(R.id.textViewResult);
        setSupportActionBar(toolbar);

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED)
        {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, 100);
        }


        initData();
        mRecyclerView = (RecyclerView) findViewById(R.id.id_recyclerview);
        buttonScan =  (Button) findViewById(R.id.buttonScan);
        buttonImprimir =  (Button) findViewById(R.id.buttonImprimir);
        buttonProductos =  (Button) findViewById(R.id.buttonProductos);
        buttonProductos =  (Button) findViewById(R.id.buttonProductos);
        buttonConfiguracion = (Button) findViewById(R.id.buttonConfiguracion);
        buttonNuevoProducto = (Button) findViewById(R.id.button_nuevo_producto);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        appToolBar = (Toolbar) findViewById(R.id.toolbar);

        appToolBar.hideOverflowMenu();
        //Drawable d = /* your drawable here */;
        //toolbar.setOverflowIcon(d);
        appToolBar.setOverflowIcon(null);



        // set up the RecyclerView
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyRecyclerViewAdapter(this, ProductosRepositorio.getInstance().productos);
        adapter.setClickListener(this);
        mRecyclerView.setAdapter(adapter);

        buttonScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, ScanQr.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                MainActivity.this.startActivity((intent));

            }
        });

        buttonProductos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ToolbarSearchActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        buttonImprimir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String BILL = armarTicket();
                Intent intent = new Intent(MainActivity.this, Printer.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("dataPrint", BILL);
                MainActivity.this.startActivity(intent);
            }
        });

        buttonConfiguracion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ConfiguracionGenerales.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                MainActivity.this.startActivity(intent);
            }
        });

        buttonNuevoProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog view = new AlertDialog.Builder((Context) MainActivity.this).create();
                LinearLayout linearLayout = new LinearLayout((Context)MainActivity.this);
                linearLayout.setOrientation(LinearLayout.VERTICAL);
                TextView textView = new TextView((Context)MainActivity.this);
                textView.setText((CharSequence)"Agregar producto extra");
                textView.setTextSize(2, 20.0f);
                textView.setTextColor(-16777216);
                linearLayout.addView((View)textView);
                final EditText editText = new EditText((Context)MainActivity.this);
                editText.setHint((CharSequence)"Nombre del producto");
                linearLayout.addView((View)editText);
                final EditText editText2 = new EditText((Context)MainActivity.this);
                editText2.setHint((CharSequence)"Precio");
                editText2.setInputType(8194);
                linearLayout.addView((View)editText2);
                final EditText editText3 = new EditText((Context)MainActivity.this);
                editText3.setHint((CharSequence)"Cantidad");
                editText3.setInputType(2);
                linearLayout.addView((View)editText3);
                Button button = new Button((Context)MainActivity.this);
                button.setPadding(50, 0, 50, 0);
                button.setText((CharSequence)"Agregar producto");
                linearLayout.addView((View)button);
                button.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder((Context) MainActivity.this);
                        if (!(editText.equals("") || editText2.getText().toString().equals("") || editText3.getText().toString().equals(""))) {
                            view.dismiss();
                            String nombre = editText.getText().toString();
                            Integer cantidad = Integer.valueOf(editText3.getText().toString());
                            Double precio = Double.valueOf(editText2.getText().toString());
                            Double total = precio * cantidad;
                            Productos nuevo_producto = new Productos(0, nombre, precio, cantidad, total, idProductoExterno);
                            ProductosRepositorio.getInstance().productos.add(nuevo_producto);

                            MainActivity.this.adapter.notifyDataSetChanged();
                            calcularSumaTotal();
                            idProductoExterno++;
                            return;
                        }
                        //this.val$dialogProdCreate.dismiss();
                        builder.setTitle(R.string.app_name);
                        builder.setMessage((CharSequence)"Por favor llene todos los datos solicitados");
                        builder.setPositiveButton((CharSequence)"Aceptar", new DialogInterface.OnClickListener(){

                            public void onClick(DialogInterface dialogInterface, int n) {
                            }
                        }).create();
                        builder.show();
                    }

                });
                view.setView((View)linearLayout);
                MainActivity.this.setMargins((View)textView, 20, 20, 20, 20);
                MainActivity.this.setMargins((View)editText, 20, 20, 20, 20);
                MainActivity.this.setMargins((View)editText2, 20, 20, 20, 20);
                MainActivity.this.setMargins((View)editText3, 20, 20, 20, 20);
                MainActivity.this.setMargins((View)button, 80, 20, 80, 20);
                view.show();

            }
        });


        readDataBase();
    }

    private void setMargins(View view, int n, int n2, int n3, int n4) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ((ViewGroup.MarginLayoutParams)view.getLayoutParams()).setMargins(n, n2, n3, n4);
            view.requestLayout();
        }
    }



    private void readDataBase()
    {
        List<String[]> rows = new ArrayList<>();
        CSVReader csvReader = new CSVReader(this, "database.csv");
        try {
            rows = csvReader.readCSV();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < rows.size(); i++) {
            Log.i("row", String.format("row %s: %s, %s, %s, %s", i, rows.get(i)[0], rows.get(i)[1], rows.get(i)[2], rows.get(i)[3] ));
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void initData() {

        Double sumaTotal = 0.0;
        for (Productos unicoProducto : ProductosRepositorio.getInstance().productos)
        {
            //Log.i("cantidadP1 ",String.valueOf(unicoProducto.getCantidad()));
            for(int l=0; l<unicoProducto.getCantidad(); l++){
                //Log.i("cantidad ",String.valueOf(unicoProducto.getCantidad()));
                sumaTotal = sumaTotal +  unicoProducto.getPrecio();
            }
        }

        textViewResult.setText(String.format("%.2f", sumaTotal));

    }

    @Override
    public void onItemClick(View view, final int position) {

        String[] colors = {"Cambiar cantidad", "Borrar Producto"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.app_name);
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (which == 0)
                {

                    final EditText editText = new EditText(MainActivity.this);
                    editText.setInputType(2);
                    editText.setGravity(1);
                    new AlertDialog.Builder(MainActivity.this).setTitle("Editar Producto").setMessage("Indique la cantidad de productos que desee").setView((View)editText).setPositiveButton("Agregar", new DialogInterface.OnClickListener(){

                        public void onClick(DialogInterface object, int n) {
                            if (String.valueOf((Object)editText.getText()).equals("")) {
                                return;
                            }
                            else
                            {
                                Productos producto =  ProductosRepositorio.getInstance().productos.get(position);

                                int cantidad = Integer.valueOf( editText.getText().toString());
                                Double total  = producto.getPrecio() * cantidad;

                                producto.setTotal(total);
                                producto.setCantidad(cantidad);

                                MainActivity.this.adapter.notifyDataSetChanged();
                                MainActivity.this.calcularSumaTotal();

                            }
                        }
                    }).setNegativeButton("Cancelar", null).create().show();
                    showKeyboard(editText);
                }
                else if (which == 1)
                {
                    ProductosRepositorio.getInstance().productos.remove(position);
                    MainActivity.this.adapter.notifyDataSetChanged();
                    MainActivity.this.calcularSumaTotal();
                }
                else
                {

                }

            }
        });
        builder.show();


    }



    public String armarTicket() {

        String BILL = "";
        SharedPreferences preferences = this.getApplicationContext().getSharedPreferences("MyPref", 0);
        preferences.edit();
        String name = preferences.getString("nombre_establecimiento", "Nombre por defecto");
        String direccion = preferences.getString("direccion_establecimiento", "Direccion por defecto");
        String contacto = preferences.getString("contacto_establecimiento", "Contacto por defecto");

        BILL = BILL + name;
        BILL = BILL + ("\n\n ");
        BILL = BILL + direccion;
        BILL = BILL + ("\n\n ");


        String prodEncabezado = (padRigth("Prod", 9, " "));
        String cantECantidad =(padRigth("Cant", 4, " "));
        String totalEncabezado = (padRigth("Precio", 8, " "));
        String precioEncabezado = padRigth("Total",8," ");;

        BILL = BILL + ("\n"+prodEncabezado+" "+cantECantidad+" "+totalEncabezado+ " " + precioEncabezado);

        for (Productos productos : ProductosRepositorio.getInstance().productos) {

            String nombre = MainActivity.padRigth(productos.getNombre(),9," ");;
            String cantidad = MainActivity.padRigth(String.valueOf(productos.getCantidad()), 4, " ");
            String precio = MainActivity.padRigth(String.valueOf(productos.getPrecio()), 8, " ");
            String total = MainActivity.padRigth(String.valueOf(productos.getTotal()), 8, " ");
            BILL = BILL + ("\n"+nombre+" "+cantidad+" "+precio + " " + total);
        }

        BILL = BILL + ("\n-----------------------------");

        BILL = BILL + ("\n\n ");
        BILL = BILL + "      Valor Total:   " + this.textViewResult.getText().toString();
        BILL = BILL + ("\n\n ");
        BILL = BILL + contacto;
        BILL = BILL + ("\n\n ");
        BILL = BILL + ("\n\n ");
        return BILL;

    }

    public static String padRigth(String string2, int n, String string3) {
        if (string2.length() == n) {
            return string2;
        }
        if (string2.length() > n) {
            return string2.substring(0, n);
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(string2);
        while (stringBuilder.length() <= n - 1) {
            stringBuilder.append(string3);
        }
        return stringBuilder.toString();
    }

    public void showKeyboard(final EditText editText) {
        editText.requestFocus();
        editText.postDelayed(new Runnable(){

            @Override
            public void run() {
                ((InputMethodManager)MainActivity.this.getSystemService("input_method")).showSoftInput((View)editText, 0);
            }
        }, 200L);
    }

    public void calcularSumaTotal() {
        Double d = 0.0;
        Iterator<Productos> iterator = ProductosRepositorio.getInstance().productos.iterator();
        block0 : do {
            boolean bl = iterator.hasNext();
            int n = 0;
            if (!bl) break;
            Productos productos = iterator.next();
            do {
                if (n >= productos.getCantidad()) continue block0;
                d = d + productos.getPrecio();
                ++n;
            } while (true);
        } while (true);
        this.textViewResult.setText((CharSequence)String.format("%.2f", d));
    }

    @Override
    public void onResume() {
        super.onResume();
        this.adapter.notifyDataSetChanged();
        this.calcularSumaTotal();
    }
}
