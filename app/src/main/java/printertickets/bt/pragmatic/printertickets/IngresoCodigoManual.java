package printertickets.bt.pragmatic.printertickets;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import android.support.v7.widget.RecyclerView.ViewHolder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import printertickets.bt.pragmatic.printertickets.Model.Productos;
import printertickets.bt.pragmatic.printertickets.Model.ProductosRepositorio;

public class IngresoCodigoManual extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    MyRecyclerViewAdapter adapter;
    private List<Productos> arrayValues;
    private Button buttonScan, buttonImprimir, buttonProductos;
    EditText textEdit;
    Button buscarProducto,seleccionarProducto;
    TextView nombreProducto,descripcionProducto;
    int posicionProductoEncontrado = 0;
    List<String[]> rows = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ingresocodigomanual);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        textEdit = findViewById(R.id.textedit);
        buscarProducto = findViewById(R.id.buttonBuscar);
        nombreProducto = findViewById(R.id.nombreProducto);
        descripcionProducto = findViewById(R.id.descripcionProducto);
        seleccionarProducto = findViewById(R.id.seleccionarProducto);

        buscarProducto.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if (IngresoCodigoManual.this.readDataBase()) {


                }
                else
                {
                    new AlertDialog.Builder(IngresoCodigoManual.this).setTitle("Agregar Producto").setMessage("No se ha encontrado el producto solicitado").setPositiveButton("Aceptar", new DialogInterface.OnClickListener(){

                        public void onClick(DialogInterface dialogInterface, int n) {
                        }
                    }).create().show();
                }
            }
        });

        this.seleccionarProducto.setOnClickListener(new View.OnClickListener(){

            public void onClick(View view) {
                IngresoCodigoManual.this.buscarIngresarProducto();
            }
        });

        nombreProducto.setVisibility(View.INVISIBLE);
        descripcionProducto.setVisibility(View.INVISIBLE);
        seleccionarProducto.setVisibility(View.INVISIBLE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public void buscarIngresarProducto() {
        if (this.readDataBase()) {
            final EditText editText = new EditText((Context)this);
            new AlertDialog.Builder((Context)this).setTitle("Agregar Producto").setMessage("\u00bfCu\u00e1l es la cantidad de productos que desea agregar?").setView((View)editText).setPositiveButton("Agregar", new DialogInterface.OnClickListener(){

                public void onClick(DialogInterface object, int n) {
                    if (String.valueOf((Object)editText.getText()).equals("")) {
                        return;
                    }
                    else
                    {

                        int id = Integer.parseInt(IngresoCodigoManual.this.rows.get(IngresoCodigoManual.this.posicionProductoEncontrado)[0]);
                        String nombre = IngresoCodigoManual.this.rows.get(IngresoCodigoManual.this.posicionProductoEncontrado)[1];
                        Double precio = Double.valueOf(IngresoCodigoManual.this.rows.get(IngresoCodigoManual.this.posicionProductoEncontrado)[2]);
                        int cantidad = Integer.valueOf(editText.getText().toString());
                        Double total = precio * cantidad;

                        Productos prod = new Productos(id,nombre,precio,cantidad,total,0);
                        agregarProducto(prod, cantidad);
                    }
                }
            }).setNegativeButton("Cancelar", null).create().show();
            editText.setInputType(2);
            editText.setGravity(1);
            this.showKeyboard(editText);
            return;
        }
        else
        {
            this.nombreProducto.setVisibility(View.INVISIBLE);
            this.descripcionProducto.setVisibility(View.INVISIBLE);
            this.seleccionarProducto.setVisibility(View.INVISIBLE);
            String string2 = this.getResources().getString(this.getResources().getIdentifier("app_name", "string", this.getPackageName()));
            new AlertDialog.Builder((Context)this).setTitle(string2).setMessage("No se encontr\u00f3 el producto solicitado").setCancelable(false).setPositiveButton("ok", new DialogInterface.OnClickListener(){

                public void onClick(DialogInterface dialogInterface, int n) {

                }
            }).show();
        }

    }

    public void agregarProducto(Productos productos, int n) {
        int n2 = 0;
        boolean bl = false;
        if (ProductosRepositorio.getInstance().productos.size() == 0) {
            ProductosRepositorio.getInstance().productos.add(productos);
        } else {
            for (Productos productos2 : ProductosRepositorio.getInstance().productos) {
                ++n2;
                if (productos2.getId() == productos.getId()) {
                    productos2.setCantidad(productos2.getCantidad() + n);
                    productos2.setTotal((double)productos2.getCantidad() * productos2.getPrecio());
                    bl = false;
                    break;
                }
                bl = true;
            }
            if (bl) {
                ProductosRepositorio.getInstance().productos.add(productos);
            }
        }
        Intent intent = new Intent(IngresoCodigoManual.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.startActivity(intent);
    }


    private boolean readDataBase()
    {

        CSVReader csvReader = new CSVReader(this, "database.csv");
        try {
            rows = csvReader.readCSV();
        } catch (IOException e) {
            e.printStackTrace();
        }

        boolean seEcontroProducto = false;
        for (int i = 0; i < rows.size(); i++) {
            Log.i("row", String.format("row %s: %s, %s, %s, %s, %s", i, rows.get(i)[0], rows.get(i)[1], rows.get(i)[2], rows.get(i)[3], rows.get(i)[4] ));

            if (textEdit.getText().toString().equals(rows.get(i)[4]))
            {
                nombreProducto.setText(rows.get(i)[1]);
                descripcionProducto.setText(rows.get(i)[3]);
                Log.i("RES ", "SE ENCONTRO PRODUCTO");
                nombreProducto.setVisibility(View.VISIBLE);
                descripcionProducto.setVisibility(View.VISIBLE);
                seleccionarProducto.setVisibility(View.VISIBLE);
                seEcontroProducto = true;
                posicionProductoEncontrado = i;

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(textEdit.getWindowToken(), 0);

                break;
            }
            else
            {

                Log.i("RES ", "NO SE ENCONTRO PRODUCTO");
                seEcontroProducto = false;

            }
        }
        return seEcontroProducto;
    }

    public void showKeyboard(final EditText ettext){
        ettext.requestFocus();
        ettext.postDelayed(new Runnable(){
                               @Override public void run(){
                                   InputMethodManager keyboard=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext,0);
                               }
                           }
                ,200);
    }

}